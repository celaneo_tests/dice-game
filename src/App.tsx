import "./styles/App.css";
import DicePage from "./pages/DicePage";

function App() {
  return <DicePage />;
}

export default App;
