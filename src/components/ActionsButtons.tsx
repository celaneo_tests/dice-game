import React from "react";

interface ActionButtonsProps {
  handleRollDice: () => void;
  handleReset: () => void;
  gameEnded: boolean;
}

const ActionButtons: React.FC<ActionButtonsProps> = ({
  handleRollDice,
  handleReset,
  gameEnded,
}) => {
  return (
    <>
      <button
        className={`decoration-slate-700 bg-black text-white px-4 py-2 rounded ${
          gameEnded ? "opacity-50 cursor-not-allowed" : ""
        }`}
        onClick={handleRollDice}
        disabled={gameEnded}
      >
        Roll Dice
      </button>
      {gameEnded && (
        <button
          className="ml-2 decoration-slate-700 bg-black text-white px-4 py-2 rounded"
          onClick={handleReset}
        >
          Reset
        </button>
      )}
    </>
  );
};

export default ActionButtons;
