import React from "react";

interface DiceProps {
  value: number;
}

const Dice: React.FC<DiceProps> = ({ value }) => {
  const diceImagePath = `/assets/dices/dice_${value}.png`;
  return (
    <div>
      <img
        src={diceImagePath}
        alt={`Dice ${value}`}
        className="mx-auto mb-4"
        style={{ width: "100%", height: "auto" }}
      />
    </div>
  );
};

export default Dice;
