import React from "react";
import Dice from "./Dice";

interface DiceContainerProps {
  firstDiceValue: number;
  secondDiceValue: number;
}

const DiceContainer: React.FC<DiceContainerProps> = ({
  firstDiceValue,
  secondDiceValue,
}) => {
  return (
    <>
      <div className="flex items-center justify-center gap-6 mb-4">
        <Dice value={firstDiceValue} />
        <Dice value={secondDiceValue} />
      </div>
      <div className="font-bold mb-4 text-3xl">
        <h1>{firstDiceValue + secondDiceValue}</h1>
      </div>
    </>
  );
};

export default DiceContainer;
