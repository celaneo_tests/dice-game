import React from "react";

interface ScoreboardProps {
  player: number;
  score: number;
}

const Scoreboard: React.FC<ScoreboardProps> = ({ player, score }) => {
  return (
    <div className="flex items-center justify-center gap-6 mt-4">
      <p className="mb-2">
        Player {player} score :<strong className="text-xl"> {score}</strong>
      </p>
    </div>
  );
};

export default Scoreboard;
