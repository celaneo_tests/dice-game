import React from "react";

interface WinnerWidgetProps {
  winner: string;
}

const WinnerWidget: React.FC<WinnerWidgetProps> = ({ winner }) => {
  return (
    <div className="mt-4">
      <p className="font-bold text-xl">Winner: {winner}</p>
    </div>
  );
};

export default WinnerWidget;
