interface InfosProps {
  currentPlayer: number;
  rounds: number;
}

const infos: React.FC<InfosProps> = ({ currentPlayer, rounds }) => {
  return (
    <div className="flex items-center justify-center gap-6 mt-4">
      <p className="mb-2">
        Current Player: Player{" "}
        <strong className="text-xl"> {currentPlayer}</strong>
      </p>
      <p className="mb-2">
        Remaining rounds : <strong className="text-xl"> {rounds}</strong>
      </p>
    </div>
  );
};

export default infos;
