import { useState } from "react";

const useDiceRoll = () => {
  const [firstDiceValue, setFirstDiceValue] = useState<number>(1);
  const [secondDiceValue, setSecondDiceValue] = useState<number>(1);

  const rollDice = () => {
    const firstValue = Math.floor(Math.random() * 6) + 1;
    const secondValue = Math.floor(Math.random() * 6) + 1;
    setFirstDiceValue(firstValue);
    setSecondDiceValue(secondValue);
  };

  return {
    firstDiceValue,
    secondDiceValue,
    rollDice,
  };
};

export default useDiceRoll;
