import { useState } from "react";

const useScore = () => {
  const [player1Score, setPlayer1Score] = useState<number>(0);
  const [player2Score, setPlayer2Score] = useState<number>(0);

  const increaseScore = (player: number, value: number) => {
    if (player === 1) {
      setPlayer1Score((prevScore) => prevScore + value);
    } else {
      setPlayer2Score((prevScore) => prevScore + value);
    }
  };

  return {
    player1Score,
    player2Score,
    increaseScore,
    setPlayer1Score,
    setPlayer2Score,
  };
};

export default useScore;
