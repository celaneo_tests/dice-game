import React, { useEffect, useState } from "react";
import useDiceRoll from "../hooks/useDiceRoll";
import Infos from "../components/infos";
import Scoreboard from "../components/Scoreboard";
import useScore from "../hooks/useScore";
import WinnerWidget from "../components/WinnerWidget";
import DiceContainer from "../components/DiceContainer";
import ActionButtons from "../components/ActionsButtons";

const DicePage: React.FC = () => {
  const [currentPlayer, setCurrentPlayer] = useState<number>(1);
  const [rounds, setRounds] = useState<number>(10);
  const [winner, setWinner] = useState("Equal");
  const { firstDiceValue, secondDiceValue, rollDice } = useDiceRoll();
  const {
    increaseScore,
    setPlayer1Score,
    setPlayer2Score,
    player1Score,
    player2Score,
  } = useScore();

  useEffect(() => {
    getWinner();
  }, [rollDice]);

  const switchPlayer = () => {
    setCurrentPlayer((prevPlayer) => (prevPlayer === 1 ? 2 : 1));
  };

  const getWinner = () => {
    if (player1Score > player2Score) {
      setWinner("Player 1");
    } else if (player2Score > player1Score) {
      setWinner("Player 2");
    }
  };

  const handleRollDice = () => {
    if (rounds > 0) {
      rollDice();
      switchPlayer();
      increaseScore(currentPlayer, firstDiceValue + secondDiceValue);
      setRounds(rounds - 1);
    }
  };

  const handleReset = () => {
    setPlayer1Score(0);
    setPlayer2Score(0);
    setRounds(10);
  };

  return (
    <div className="text-center mt-8">
      <DiceContainer
        firstDiceValue={firstDiceValue}
        secondDiceValue={secondDiceValue}
      />

      <ActionButtons
        handleRollDice={handleRollDice}
        gameEnded={rounds === 0}
        handleReset={handleReset}
      />

      <div className="flex items-center justify-center gap-6 mb-4">
        <Scoreboard player={1} score={player1Score} />
        <Scoreboard player={2} score={player2Score} />
      </div>

      {rounds > 0 && <Infos currentPlayer={currentPlayer} rounds={rounds} />}

      {rounds <= 0 && <WinnerWidget winner={winner} />}
    </div>
  );
};

export default DicePage;
